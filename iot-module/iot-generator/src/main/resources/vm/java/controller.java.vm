package ${packageName}.controller;

import java.util.List;


import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import cc.iotkit.common.log.annotation.Log;
import cc.iotkit.common.web.core.BaseController;
import cc.iotkit.common.api.PageRequest;
import cc.iotkit.common.api.Paging;
import cc.iotkit.common.api.Request;
import cc.iotkit.common.validate.AddGroup;
import cc.iotkit.common.validate.EditGroup;
import cc.iotkit.common.log.enums.BusinessType;
import cc.iotkit.common.excel.utils.ExcelUtil;
import ${packageName}.dto.vo.${ClassName}Vo;
import ${packageName}.dto.bo.${ClassName}Bo;
import ${packageName}.service.I${ClassName}Service;


/**
 * ${functionName}
 *
 * @author ${author}
 * @date ${datetime}
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/${moduleName}/${businessName}")
public class ${ClassName}Controller extends BaseController {

    private final I${ClassName}Service ${className}Service;

    /**
     * 查询${functionName}列表
     */
    @SaCheckPermission("${permissionPrefix}:list")
    @PostMapping("/list")
    @ApiOperation("查询${functionName}列表")
#if($table.crud || $table.sub)
    public Paging<${ClassName}Vo> list( PageRequest<${ClassName}Bo> pageQuery) {
        return ${className}Service.queryPageList(pageQuery);
    }
#elseif($table.tree)
    public List<${ClassName}Vo> list(@RequestBody Request<${ClassName}Bo> query) {
        List<${ClassName}Vo> list = ${className}Service.queryList(query.getData());
        return list;
    }
#end

    /**
     * 导出${functionName}列表
     */
    @ApiOperation("导出${functionName}列表")
    @SaCheckPermission("${permissionPrefix}:export")
    @Log(title = "${functionName}", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(${ClassName}Bo bo, HttpServletResponse response) {
        List<${ClassName}Vo> list = ${className}Service.queryList(bo);
        ExcelUtil.exportExcel(list, "${functionName}", ${ClassName}Vo.class, response);
    }

    /**
     * 获取${functionName}详细信息
     *
     */
    @SaCheckPermission("${permissionPrefix}:query")
    @PostMapping("/getDetail")
    @ApiOperation("获取${functionName}详细信息")
    public ${ClassName}Vo getDetail(@Validated @RequestBody Request<Long> request) {
        return ${className}Service.queryById(request.getData());
    }

    /**
     * 新增${functionName}
     */
    @SaCheckPermission("${permissionPrefix}:add")
    @Log(title = "${functionName}", businessType = BusinessType.INSERT)
    @PostMapping(value = "/add")
    @ApiOperation("新增${functionName}")
    public Long add(@Validated(AddGroup.class) @RequestBody Request<${ClassName}Bo> request) {
        return ${className}Service.insertByBo(request.getData());
    }

    /**
     * 修改${functionName}
     */
    @SaCheckPermission("${permissionPrefix}:edit")
    @Log(title = "${functionName}", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ApiOperation("修改${functionName}")
    public boolean edit(@Validated(EditGroup.class) @RequestBody  Request<${ClassName}Bo> request) {
        return ${className}Service.updateByBo(request.getData());
    }

    /**
     * 删除${functionName}
     *
     */
    @SaCheckPermission("${permissionPrefix}:remove")
    @Log(title = "${functionName}", businessType = BusinessType.DELETE)
    @PostMapping("/delete")
    @ApiOperation("删除${functionName}")
    public boolean remove(@Validated @RequestBody Request<List<Long>> query) {
        return ${className}Service.deleteWithValidByIds(query.getData(), true);
    }
}
