//package cc.iotkit.common.tenant.manager;
//
//import cc.iotkit.common.constant.GlobalConstants;
//import cc.iotkit.common.redis.manager.PlusSpringCacheManager;
//import cc.iotkit.common.tenant.helper.TenantHelper;
//import cc.iotkit.common.utils.StringUtils;
//import org.springframework.cache.Cache;
//
///**
// * 重写 cacheName 处理方法 支持多租户
// *
// * @author Lion Li
// */
//public class TenantSpringCacheManager extends PlusSpringCacheManager {
//
//    public TenantSpringCacheManager() {
//    }
//
//    @Override
//    public Cache getCache(String name) {
//        if (StringUtils.contains(name, GlobalConstants.GLOBAL_REDIS_KEY)) {
//            return super.getCache(name);
//        }
//        String tenantId = TenantHelper.getTenantId();
//        if (StringUtils.startsWith(name, tenantId)) {
//            // 如果存在则直接返回
//            return super.getCache(name);
//        }
//        return super.getCache(tenantId + ":" + name);
//    }
//
//}
