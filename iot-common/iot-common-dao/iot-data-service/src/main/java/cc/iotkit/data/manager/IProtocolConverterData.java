package cc.iotkit.data.manager;

import cc.iotkit.data.IOwnedData;
import cc.iotkit.model.protocol.ProtocolConverter;

public interface IProtocolConverterData extends IOwnedData<ProtocolConverter, String> {

}
